package com.practiceselenium.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class practiceseleniumTalktea {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		//Create firefox driver
		WebDriver driver = new FirefoxDriver();
		
		//open http://www.practiceselenium.com/menu.html
		driver.get("http://www.practiceselenium.com/let-s-talk-tea.html");
		Thread.sleep(5000);
		
		//not completing the form, imemediately pressing button submit
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		Thread.sleep(6000);
		
		//completing the form with incorect email format
		driver.findElement(By.name("name")).sendKeys("yafie");
		Thread.sleep(2000);
		driver.findElement(By.name("email")).sendKeys("tes.abc");
		Thread.sleep(2000);
		driver.findElement(By.name("subject")).sendKeys("testing");
		Thread.sleep(2000);
		driver.findElement(By.name("message")).sendKeys("testing");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@type='submit']")).click();
		Thread.sleep(6000);
		
		if(driver.findElement(By.id("msg_78ea690540a24bd8b9dcfbf99e999fea")).getAttribute("style")!=""){
			//completing the form with incorect email format
			driver.findElement(By.name("email")).clear();
			driver.findElement(By.name("email")).sendKeys("tes.abc@gmail.com");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//input[@type='submit']")).click();
			Thread.sleep(6000);
			
			if(driver.findElement(By.cssSelector("#form_78ea690540a24bd8b9dcfbf99e999fea .form-body")).getAttribute("style")!=""){
				System.out.println("Event Handling Work Well");
			}
			//close firefox browser
			driver.close();
		}else{
			System.out.println("Event Handling NOT Work Well");
			driver.close();
		}
			
	}

}
