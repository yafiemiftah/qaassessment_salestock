package com.practiceselenium.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class practiceseleniumCheckout {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		//Create firefox driver
		WebDriver driver = new FirefoxDriver();
		
		//open http://www.practiceselenium.com/menu.html
		driver.get("http://www.practiceselenium.com/menu.html");
		Thread.sleep(4000);
		
		//select one from the menu (i am choose oolong tea and click checkout)
		driver.findElement(By.id("wsb-button-00000000-0000-0000-0000-000451961556")).click();
		Thread.sleep(6000);
		
		//to page checkout and immediately pressing the button without complete the form
		driver.findElement(By.cssSelector("button.btn.btn-primary")).click();
		Thread.sleep(5000);
		
		//close firefox browser
		driver.close();
	}

}
